package library.testing;
import library.entities;

import java.beans.Transient;

import org.junit.Test;
    
public class LibraryTest {
        
    @Test
    public void patronListTest() {
            List<IPatron> getPatronList() {
            return new ArrayList<IPatron>(patrons.values());
        }
    }
    @Test
    public void loanTest() {
        ILoan loan = loanHelper.makeLoan(book, patron);
		return loan;
    }
    @Test
    public void getDaysDifferenceTest() {
        public synchronized long getDaysDifference(Date targetDate) {
            long diffMilliseconds = getDate().getTime() - targetDate.getTime();
            long diffDays = diffMilliseconds / MILLIS_PER_DAY;
            return diffDays;
        }
    }
}
